<?php
/*
Plugin Name: divulgacine.com SCHEMA
Plugin URI: https://divulgacine.com/
Description: Plugin de schema de divulgacine.com
Version: 1.0
Author: Flavia Bernárdez
Author URI: https://flabernardez.com/
License: GPLv2 or later
Text Domain: di-schema
*/

/* Add schema event to front page */
add_action('wp_head', 'fla_schema_event_fp');
function fla_schema_event_fp(){
	if ( is_front_page() ) {

		global $wp;
		global $post;

		echo '<script type="application/ld+json">
            {
                "@context": "http://schema.org/",
                "@type": "Event",
                "name": "'. get_the_title() .'",
                "startDate": "2018-11-30T17:00+01:00",
                "location": {
                    "@type": "Place",
                    "name": "Casa de la música, Las Cigarreras",
                    "address": {
                        "@type": "PostalAddress",
                        "streetAddress": "Calle San Carlos, 78",
                        "addressLocality": "Alicante",
                        "postalCode": "03013",
                        "addressRegion": "Alicante",
                        "addressCountry": "España"
                    }
                },
                "image": "https://divulgacine.com/wp-content/assets/rrss_facebook.jpg",
                "description": "'. get_the_excerpt() .'",
                "endDate": "2018-12-01T20:30+01:00",
                "offers": {
                    "@type": "Offer",
                    "url": "https://divulgacine.com/entradas/",
                    "price": "0",
                    "priceCurrency": "EUR",
                    "availability": "http://schema.org/InStock",
                    "validFrom": "2018-11-12T16:00+01:00"
                },
                "performer": [{
                    "@type": "Person",
                    "name": "Carlos Boyero"
                    },{
                    "@type": "Person",
                    "name": "Oti Rodríguez Marchante"
                    },{
                    "@type": "Person",
                    "name": "Jerónimo José Martín"
                    },{
                    "@type": "Person",
                    "name": "Miguel Losada"
                    },{
                    "@type": "Person",
                    "name": "Mariano Sánchez Soler"
                    },{
                    "@type": "Person",
                    "name": "John Tones"
                    },{
                    "@type": "Person",
                    "name": "Antonio Martínez"
                    },{
                    "@type": "Person",
                    "name": "Valentina Morillo"
                    },{
                    "@type": "Person",
                    "name": "Juan Zavala"
                    },{
                    "@type": "Person",
                    "name": "Gonzalo Eulogio"
                    },{
                    "@type": "Person",
                    "name": "Antonio Sempere"
                    },{
                    "@type": "Person",
                    "name": "Augusto González"
                    },{
                    "@type": "Person",
                    "name": "CJ Navas"
                    },{
                    "@type": "Person",
                    "name": "Juan Carlos Vizcaíno"
                    }
                    ,{
                    "@type": "Person",
                    "name": "Luis López Belda",
                    "sameAs": "http://luislobelda.com/"
                    }
                ]   
            }
            </script>
        ';

    }
};