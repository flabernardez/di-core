<?php
/*
Plugin Name: divulgacine.com CORE
Plugin URI: https://divulgacine.com/
Description: Plugin del core de divulgacine.com
Version: 1.0
Author: Flavia Bernárdez
Author URI: https://flabernardez.com/
License: GPLv2 or later
Text Domain: di-core
*/

function fla_soporte_svg($svg = array()){

	$svg['svg'] = 'image/svg+xml';
	return $svg;

}

add_filter('upload_mimes', 'fla_soporte_svg');

function fla_soporte_zip($zip = array()){

	$zip['zip'] = 'application/zip';
	return $zip;

}

add_filter('upload_mimes', 'fla_soporte_zip');